(function ($) {
  'use strict';

  /**
   * Site javascripts
   * ===
   */

  // Wrap video embeds in Flexible Container
  $('iframe[src*="youtube.com"]:not(.not-responsive), iframe[src*="vimeo.com"]:not(.not-responsive)')
    .attr( 'frameborder', 0 )
    .wrap('<div class="flexible-container"></div>');

  // Resize Maps
  $('iframe[src*="google.com/map"]:not(.not-responsive)')
    .attr( 'frameborder', 0 )
    .wrap('<div class="flexible-container"></div>');

  /**
   * Hero Pause / Play Controls
   */
  
  var $hero_video_button = $('.js-hero-controls');
  var $hero_bkg_video    = $('.hero-bkg-video');
  var is_video_paused    =  Cookies.get('is_video_paused');

  $hero_bkg_video.each(function () {
    var svg_icon = $hero_video_button.find('svg use');

    if ( is_video_paused == 'true' ) {
      this.pause();
      svg_icon.attr( 'href', '#icon-circle-play' );
    }
  });

  $hero_video_button.on('click', function () {
    var svg_icon = $(this).find('svg use');

    $hero_bkg_video.each(function () {
      // check if video is playing
      if ( ! this.paused ) {
        this.pause();
        svg_icon.attr( 'href', '#icon-circle-play' );
        Cookies.set('is_video_paused', 'true', { expires: 15 });
      } else {
        this.play();
        svg_icon.attr( 'href', '#icon-circle-pause' );
        Cookies.set('is_video_paused', 'false', { expires: 15 });
      }
    });
    return false;
  });


})(jQuery);
