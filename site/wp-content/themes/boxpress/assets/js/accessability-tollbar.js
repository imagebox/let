(function ($) {
  'use strict';

  /**
   * Accessible Toolbar Controls
   */
  
  var is_toolbar_expanded = Cookies.get('is_accessibility_toolbar');
  var $toolbar_toggle     = $('.toolbar-toggle-button');
  var $toolbar_body       = $('.accessibility-toolbar-body');

  // Prep Toolbar - Open
  if ( is_toolbar_expanded == 'true' || is_toolbar_expanded === undefined ) {
    open_toolbar( $toolbar_toggle, $toolbar_body );

  // Closed State
  } else if ( is_toolbar_expanded == 'false' ) {
    close_toolbar( $toolbar_toggle, $toolbar_body );
  }

  $('.toolbar-toggle-button').on('click', function () {
    var $this = $(this);
    var $toolbar = $this.closest('.accessibility-toolbar');
    var $toolbar_body = $toolbar.find('.accessibility-toolbar-body');

    $toolbar.toggleClass( 'is-closed' );

    // Closed State
    if ( $toolbar.hasClass( 'is-closed' )) {
      close_toolbar( $this, $toolbar_body );

    // Open State
    } else {
      open_toolbar( $this, $toolbar_body );
    }

    return false;
  });

  // Open Toolbar
  function open_toolbar( $button, $slide ) {
    $button.text('Close');
    $button.attr( 'aria-expanded', 'true' );
    $slide.attr( 'aria-hidden', 'false' );
    Cookies.set( 'is_accessibility_toolbar', 'true', { expires: 15 });
  }

  // Close Toolbar
  function close_toolbar( $button, $slide ) {
    $button.text( 'Open' );
    $button.attr( 'aria-expanded', 'false' );
    $slide.attr( 'aria-hidden', 'true' );
    Cookies.set( 'is_accessibility_toolbar', 'false', { expires: 15 });
  }


  /**
   * Font-size Changer
   */
  
  var $font_size_button_increase  = $('.js-font-size-increase');
  var $font_size_button_decrease  = $('.js-font-size-decrease');
  var $font_size_button_reset     = $('.js-font-size-reset');
  var $body_el = $('body');

  $font_size_button_increase.on('click', function () {
    var current_font_size = parseInt( $body_el.css( 'font-size' ));
    var new_font_size = current_font_size + 2;

    $body_el.css( 'font-size', new_font_size + 'px' );
    Cookies.set( 'font_size', new_font_size, { expires: 15 });
    return false;
  });

  $font_size_button_decrease.on('click', function () {
    var current_font_size = parseInt( $body_el.css( 'font-size' ));
    var new_font_size = current_font_size - 2;

    $body_el.css( 'font-size', new_font_size + 'px' );
    Cookies.set( 'font_size', new_font_size, { expires: 15 });
    return false;
  });

  $font_size_button_reset.on('click', function () {
    var new_font_size = 16;

    $body_el.css( 'font-size', new_font_size + 'px' );
    Cookies.set( 'font_size', new_font_size, { expires: 15 });
    return false;
  });

})(jQuery);
