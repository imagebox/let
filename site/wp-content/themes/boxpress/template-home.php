<?php
/**
 * Template Name: Homepage
 *
 * Page template to display the homepage.
 *
 * @package boxpress
 */
get_header(); ?>

  <article class="homepage">

    <?php // Hero ?>
    <?php get_template_part( 'template-parts/homepage/homepage-hero' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-alert-split' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-split-block' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-columns' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-callout' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-carousel' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-split-block-two' ); ?>



  </article>

<?php get_footer(); ?>
