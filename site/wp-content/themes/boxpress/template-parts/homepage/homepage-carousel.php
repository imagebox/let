<?php
/**
 * Displays the Slideshow layout
 *
 * @package boxpress
 */

  $home_carousel_photo  = get_field( 'home_carousel_photo' );
  $home_carousel_mobile_background = get_field( 'home_carousel_mobile_background' );
?>

<section class="section homepage-quote-section">
  <?php if ( $home_carousel_photo ) : ?>
    <img class="homepage-quote-section-desktop-bkg" draggable="false" aria-hidden="true"
      src="<?php echo esc_url( $home_carousel_photo['url'] ); ?>"
      width="<?php echo esc_attr( $home_carousel_photo['width'] ); ?>"
      height="<?php echo esc_attr( $home_carousel_photo['height'] ); ?>"
      alt="<?php echo esc_attr( $home_carousel_photo['alt'] ); ?>">
  <?php endif; ?>

  <?php if ( $home_carousel_mobile_background ) : ?>
    <img class="homepage-quote-section-mobile-bkg" draggable="false" aria-hidden="true"
      src="<?php echo esc_url( $home_carousel_mobile_background['url'] ); ?>"
      width="<?php echo esc_attr( $home_carousel_mobile_background['width'] ); ?>"
      height="<?php echo esc_attr( $home_carousel_mobile_background['height'] ); ?>"
      alt="<?php echo esc_attr( $home_carousel_mobile_background['alt'] ); ?>">
  <?php endif; ?>

  <div class="wrap">
    <?php if ( have_rows( 'quote_carousel' )) : ?>
      <div class="home-carousel">
        <div class="js-carousel">
          <?php while ( have_rows( 'quote_carousel' )) : the_row(); ?>
            <?php
              $quote  = get_sub_field( 'quote' );
              $name  = get_sub_field( 'name' );
             ?>

            <div class="carousel-slide">
              <div class="slide-content">
                <svg width="53px" height="53px" viewBox="0 0 53 53" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="LET_Homepage_v17" transform="translate(-775.000000, -2568.000000)">
                            <g id="Group-22" transform="translate(775.000000, 2568.000000)">
                                <g id="Group-13">
                                    <circle id="Mask" fill="#7A1A00" cx="26.5" cy="26.5" r="26.5"></circle>
                                    <g id="Group-14" transform="translate(19.017500, 18.970000)" fill="#FFFFFF" fill-rule="nonzero">
                                        <path d="M14.637,1.722 C13.6529951,2.48733716 12.8945027,3.25949611 12.3615,4.0385 C11.8284973,4.8175039 11.562,5.54866325 11.562,6.232 C11.6713339,6.17733306 11.8148324,6.14316674 11.9925,6.1295 C12.1701676,6.11583327 12.3273326,6.109 12.464,6.109 C13.0926698,6.109 13.6188312,6.36183081 14.0425,6.8675 C14.4661688,7.3731692 14.678,7.99499631 14.678,8.733 C14.678,9.63500451 14.3773363,10.3866637 13.776,10.988 C13.1746637,11.5893363 12.4230045,11.89 11.521,11.89 C10.5643285,11.89 9.8126694,11.5415035 9.266,10.8445 C8.7193306,10.1474965 8.446,9.21133921 8.446,8.036 C8.446,6.64199303 8.86966243,5.2616735 9.717,3.895 C10.5643376,2.5283265 11.8079918,1.23000615 13.448,1.63424829e-13 L14.637,1.722 Z" id="Path"></path>
                                        <path d="M6.191,1.722 C5.20699508,2.48733716 4.44850267,3.25949611 3.9155,4.0385 C3.38249734,4.8175039 3.116,5.54866325 3.116,6.232 C3.22533388,6.17733306 3.36883244,6.14316674 3.5465,6.1295 C3.72416756,6.11583327 3.88133265,6.109 4.018,6.109 C4.64666981,6.109 5.17283122,6.36183081 5.5965,6.8675 C6.02016879,7.3731692 6.232,7.99499631 6.232,8.733 C6.232,9.63500451 5.93133634,10.3866637 5.33,10.988 C4.72866366,11.5893363 3.97700451,11.89 3.075,11.89 C2.11832855,11.89 1.3666694,11.5415035 0.82,10.8445 C0.2733306,10.1474965 -2.84217094e-14,9.21133921 -2.84217094e-14,8.036 C-2.84217094e-14,6.64199303 0.42366243,5.2616735 1.271,3.895 C2.11833757,2.5283265 3.3619918,1.23000615 5.002,1.63424829e-13 L6.191,1.722 Z" id="Path"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
                <?php if ( $quote ) : ?>
                  <h2><?php echo $quote; ?></h2>
                <?php endif; ?>

                <?php if ( $name ) : ?>
                  <span>- <?php echo $name; ?></span>
                <?php endif; ?>
              </div>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
</section>
