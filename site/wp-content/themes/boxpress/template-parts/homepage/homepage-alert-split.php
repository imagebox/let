<?php
  $home_alert_pic = get_field('home_alert_pic');
  $home_alert_heading = get_field('home_alert_heading');
  $home_alert_text = get_field('home_alert_text');
  $home_alert_link = get_field('home_alert_link');

  $home_split_heading = get_field('home_split_heading');
  $home_split_text = get_field('home_split_text');
  $home_split_link = get_field('home_split_link');
  $home_split_pic = get_field('home_split_pic');
 ?>

<section class="section home-alert-split-section">
 <div class="wrap">
  <div class="alert-box">
    <div class="row">
      <div class="col-xs-12 col-lg-4">
        <div class="photo-box">
          <?php if ( $home_alert_pic ) : ?>
            <img src="<?php echo esc_url( $home_alert_pic['url'] ); ?>"
              width="<?php echo esc_attr( $home_alert_pic['width'] ); ?>"
              height="<?php echo esc_attr( $home_alert_pic['height'] ); ?>"
              alt="<?php echo esc_attr( $home_alert_pic['alt'] ); ?>">
          <?php endif; ?>
        </div>
      </div>
      <div class="col-xs-12 col-lg-8">
        <div class="content-box">
         <div class="content">
           <h2><?php echo $home_alert_heading; ?></h2>

           <?php if ( $home_split_text ) : ?>
             <p><?php echo $home_alert_text; ?></p>
           <?php endif; ?>

           <?php if ( $home_alert_link ) : ?>
            <?php
              $home_alert_link_target = ! empty( $home_alert_link['target'] ) ? $home_alert_link['target'] : '_self';
            ?>
             <a class="button"
               href="<?php echo esc_url( $home_alert_link['url'] ); ?>"
               target="<?php echo esc_attr( $home_alert_link_target ); ?>">
               <?php echo $home_alert_link['title']; ?>
             </a>
           <?php endif; ?>
         </div>
        </div>
      </div>
    </div>
  </div>
 </div>
 <div class="split-box">
   <div class="row">
     <div class="col-xs-12 col-lg-6">
       <div class="content-box">
        <div class="content">
          <h2><?php echo $home_split_heading; ?></h2>

          <?php if ( $home_split_text ) : ?>
            <p><?php echo $home_split_text; ?></p>
          <?php endif; ?>

          <?php if ( $home_split_link ) : ?>
            <?php
              $home_split_link_target = ! empty( $home_split_link['target'] ) ? $home_split_link['target'] : '_self';
            ?>
            <a class="button"
              href="<?php echo esc_url( $home_split_link['url'] ); ?>"
              target="<?php echo esc_attr( $home_split_link_target ); ?>">
              <?php echo $home_split_link['title']; ?>
            </a>
          <?php endif; ?>
        </div>
       </div>
     </div>
     <div class="col-xs-12 col-lg-6">
      <div class="photo-box">
        <?php if ( $home_split_pic ) : ?>
          <img src="<?php echo esc_url( $home_split_pic['url'] ); ?>"
            width="<?php echo esc_attr( $home_split_pic['width'] ); ?>"
            height="<?php echo esc_attr( $home_split_pic['height'] ); ?>"
            alt="<?php echo esc_attr( $home_split_pic['alt'] ); ?>">
        <?php endif; ?>
      </div>
     </div>
   </div>
 </div>
</section>
