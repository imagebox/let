<?php

$home_callout_heading = get_field('home_callout_heading');
$home_callout_text = get_field('home_callout_text');
$home_callout_link = get_field('home_callout_link');

 ?>


<section class="section home-callout-section">
  <div class="wrap">
    <div class="callout-box">
      <?php if ( $home_callout_heading ) : ?>
        <h2><?php echo $home_callout_heading; ?></h2>
      <?php endif; ?>

      <?php if ( $home_callout_text ) : ?>
        <p><?php echo $home_callout_text; ?></p>
      <?php endif; ?>

      <?php if ( $home_callout_link ) : ?>
        <?php
          $home_callout_link_target = ! empty( $home_callout_link['target'] ) ? $home_callout_link['target'] : '_self';
        ?>
        <a class="button button--white"
          href="<?php echo esc_url( $home_callout_link['url'] ); ?>"
          target="<?php echo esc_attr( $home_callout_link_target ); ?>">
        <?php echo $home_callout_link['title']; ?>
      </a>
    <?php endif; ?>
    </div>
  </div>
</section>
