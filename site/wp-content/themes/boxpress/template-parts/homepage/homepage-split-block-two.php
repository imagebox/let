<?php

  $home_callout_two_pic = get_field('home_callout_two_pic');
  $home_callout_two_pic_mobile = get_field('home_callout_two_pic_mobile');
  $home_callout_two_heading = get_field('home_callout_two_heading');
  $home_callout_two_text = get_field('home_callout_two_text');
  $home_callout_two_link = get_field('home_callout_two_link');

?>

<section class="homepage-split-two-section">
    <div class="row">
      <div class="col-xs-12 col-lg-6">
        <div class="photo-box">
          <?php if ( $home_callout_two_pic ) : ?>
            <img class="homepage-split-two-section-desktop-bkg"
              src="<?php echo esc_url( $home_callout_two_pic['url'] ); ?>"
              width="<?php echo esc_attr( $home_callout_two_pic['width'] ); ?>"
              height="<?php echo esc_attr( $home_callout_two_pic['height'] ); ?>"
              alt="<?php echo esc_attr( $home_callout_two_pic['alt'] ); ?>">
          <?php endif; ?>
          <?php if ( $home_callout_two_pic_mobile ) : ?>
            <img class="homepage-split-two-section-mobile-bkg"
              src="<?php echo esc_url( $home_callout_two_pic_mobile['url'] ); ?>"
              width="<?php echo esc_attr( $home_callout_two_pic_mobile['width'] ); ?>"
              height="<?php echo esc_attr( $home_callout_two_pic_mobile['height'] ); ?>"
              alt="<?php echo esc_attr( $home_callout_two_pic_mobile['alt'] ); ?>">
          <?php endif; ?>
        </div>
      </div>
      <div class="col-xs-12 col-lg-6">
        <div class="callout-box">
          <div class="content">
            <?php if ( $home_callout_two_heading ) : ?>
              <h2><?php echo $home_callout_two_heading; ?></h2>
            <?php endif; ?>

            <?php if ( $home_callout_two_text ) : ?>
              <p><?php echo $home_callout_two_text; ?></p>
            <?php endif; ?>

            <?php if ( $home_callout_two_link ) : ?>
              <?php
                $home_callout_two_link_target = ! empty( $home_callout_two_link['target'] ) ? $home_callout_two_link['target'] : '_self';
              ?>
              <a class="button"
                href="<?php echo esc_url( $home_callout_two_link['url'] ); ?>"
                target="<?php echo esc_attr( $home_callout_two_link_target ); ?>">
              <?php echo $home_callout_two_link['title']; ?>
            </a>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
</section>
