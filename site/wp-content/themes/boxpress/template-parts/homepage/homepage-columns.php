<section class="section home-column-section">
 <div class="wrap">
   <div class="row">
   <?php if ( have_rows( 'homepage_columns' )) : ?>
       <?php while ( have_rows( 'homepage_columns' )) : the_row();

         $home_column_icon = get_sub_field('home_column_icon');
         $home_column_heading = get_sub_field('home_column_heading');
         $home_column_text = get_sub_field('home_column_text');
         $home_column_link = get_sub_field('home_column_link');

        ?>

        <div class="col-xs-12 col-lg">
          <div class="home-column-content">
              <div class="column-body">
                <img src="<?php echo esc_url( $home_column_icon['url'] ); ?>"
                  width="<?php echo esc_attr( $home_column_icon['width'] ); ?>"
                  height="<?php echo esc_attr( $home_column_icon['height'] ); ?>"
                  alt="<?php echo esc_attr( $home_column_icon['alt'] ); ?>">

                  <h2><?php echo $home_column_heading; ?></h2>

                  <?php if ( $home_column_text ) : ?>
                    <p><?php echo $home_column_text; ?></p>
                  <?php endif; ?>
              </div>

              <?php if ( $home_column_link ) : ?>
                <?php
                  $home_column_link_target = ! empty( $home_column_link['target'] ) ? $home_column_link['target'] : '_self';
                ?>
                <a class="button"
                  href="<?php echo esc_url( $home_column_link['url'] ); ?>"
                  target="<?php echo esc_attr( $home_column_link_target ); ?>">
                  <?php echo $home_column_link['title']; ?>
                </a>
              <?php endif; ?>
          </div>
        </div>
       <?php endwhile; ?>
   <?php endif; ?>
 </div>
 </div>
</section>
