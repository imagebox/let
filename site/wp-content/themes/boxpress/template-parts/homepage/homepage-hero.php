<?php
/**
 * Displays the Hero layout
 *
 * Easily convertible into a sideshow by enabling multiple
 * rows in the repeater.
 *
 * @package boxpress
 */
?>
<?php if ( have_rows( 'homepage_hero' )) : ?>
  <?php while ( have_rows( 'homepage_hero' )) : the_row();
      $hero_heading     = get_sub_field( 'hero_heading' );
      $hero_subheading  = get_sub_field( 'hero_subheading' );
      $hero_link        = get_sub_field( 'hero_link' );
      $hero_background  = get_sub_field( 'hero_background' );
      $mobile_background_image = get_sub_field( 'mobile_background_image' );
      $hero_background_image = get_sub_field( 'hero_background_image' );
      $background_video = get_sub_field( 'background_video' );
      $background_video_cover_image = get_sub_field( 'background_video_cover_image' );
    ?>

    <section class="hero <?php echo $hero_background; ?>">
      <div class="wrap">
        <div class="hero-box">
          <div class="hero-content">

            <h1><?php echo $hero_heading; ?></h1>

            <?php if ( ! empty( $hero_subheading )) : ?>
              <p><?php echo $hero_subheading; ?></p>
            <?php endif; ?>

            <?php if ( $hero_link ) : ?>
              <?php
                $hero_link_target = ! empty( $hero_link['target'] ) ? $hero_link['target'] : '_self';
              ?>
              <a class="button button--reverse"
                href="<?php echo esc_url( $hero_link['url'] ); ?>"
                target="<?php echo esc_attr( $hero_link_target ); ?>">
                <?php echo $hero_link['title']; ?>
              </a>
            <?php endif; ?>

          </div>
        </div>
      </div>

      <?php if ( $background_video && $hero_background === 'background-video' ) : ?>
        <video class="hero-bkg-video" aria-hidden="true"
          autoplay muted loop playsinline
          width="1600" height="677"
          <?php if ( $background_video_cover_image ) : ?>
            poster="<?php echo esc_url( $background_video_cover_image['url'] ); ?>"
          <?php endif; ?>
          src="<?php echo esc_url( $background_video['url'] ); ?>"></video>

        <button class="js-hero-controls hero-video-controls" type="button" aria-hidden="true">
          <span class="vh"><?php _e('Pause Background Video', 'boxpress'); ?></span>
          <svg class="icon-circle-pause-svg" width="44" height="44" focusable="false">
            <use href="#icon-circle-pause" />
          </svg>
        </button>

      <?php elseif ( $hero_background_image && $hero_background === 'background-image' ) : ?>
        <img class="hero-bkg-image" draggable="false" aria-hidden="true"
          src="<?php echo esc_url( $hero_background_image['url'] ); ?>"
          width="<?php echo esc_attr( $hero_background_image['width'] ); ?>"
          height="<?php echo esc_attr( $hero_background_image['height'] ); ?>"
          alt="">
      <?php endif; ?>

      <?php if ( $mobile_background_image && $hero_background === 'background-image' ) : ?>
        <img class="mobile-hero-bkg-image" draggable="false" aria-hidden="true"
          src="<?php echo esc_url( $mobile_background_image['url'] ); ?>"
          width="<?php echo esc_attr( $mobile_background_image['width'] ); ?>"
          height="<?php echo esc_attr( $mobile_background_image['height'] ); ?>"
          alt="">
      <?php endif; ?>
    </section>

  <?php endwhile; ?>
<?php endif; ?>
