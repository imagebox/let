<?php
/**
 * Displays the page banner
 *
 * @package boxpress
 */

$banner_title         = get_the_title();
$banner_image_url     = '';
$banner_image_width   = '';
$banner_image_height  = '';
$default_banner       = get_field( 'default_banner_image', 'option' );

if ( $default_banner ) {
  $banner_image_url     = $default_banner['url'];
  $banner_image_width   = $default_banner['width'];
  $banner_image_height  = $default_banner['height'];
}

// Set top page title and banner image
if ( 0 !== $post->post_parent ) {
  $post_ancestors = get_post_ancestors( $post->ID );
  $post_id = end( $post_ancestors );
  $banner_title = get_the_title( $post_id );
  $top_featured_image = get_the_post_thumbnail( $post_id, 'full', array(
    'class' => 'banner-image',
    'draggable' => 'false',
  ));

  if ( ! empty( $top_featured_image )) {
    $banner_image = $top_featured_image;
  }
}

if ( has_post_thumbnail() ) {
  $banner_image = get_the_post_thumbnail( get_the_ID(), 'full', array(
    'class' => 'banner-image',
    'draggable' => 'false',
  ));
}

?>
<header class="banner">
  <div class="banner-col banner-col--2">
    <?php if ( ! empty( $banner_image )) : ?>
      <?php echo $banner_image; ?>
    <?php elseif ( ! empty( $banner_image_url ) ) : ?>
      <img class="banner-image" src="<?php echo $banner_image_url; ?>"
        width="<?php echo $banner_image_width; ?>"
        height="<?php echo $banner_image_height; ?>"
        draggable="false"
        alt="">
    <?php endif; ?>
  </div>
  <div class="banner-col banner-col--1">
    <div class="banner-content-wrap">
      <div class="banner-title">
        <span class="h1 banner-heading"><?php echo $banner_title; ?></span>
      </div>
    </div>
  </div>
</header>
