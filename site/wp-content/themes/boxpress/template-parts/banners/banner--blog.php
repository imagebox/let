<?php
/**
 * Displays the blog banner
 *
 * @package boxpress
 */

$banner_title         = get_the_title( get_option( 'page_for_posts', true ));
$banner_image_url     = '';
$banner_image_width   = '';
$banner_image_height  = '';
$default_banner       = get_field( 'default_banner_image', 'option' );
$blog_banner          = get_field( 'blog_banner_image', 'option' );

if ( $blog_banner ) {
  $banner_image_url     = $blog_banner['url'];
  $banner_image_width   = $blog_banner['width'];
  $banner_image_height  = $blog_banner['height'];
} elseif ( $default_banner ) {
  $banner_image_url     = $default_banner['url'];
  $banner_image_width   = $default_banner['width'];
  $banner_image_height  = $default_banner['height'];
}

?>
<header class="banner">
  <div class="banner-col banner-col--2">
    <?php if ( ! empty( $banner_image_url ) ) : ?>
      <img class="banner-image" src="<?php echo $banner_image_url; ?>"
        width="<?php echo $banner_image_width; ?>"
        height="<?php echo $banner_image_height; ?>"
        draggable="false"
        alt="">
    <?php endif; ?>
  </div>
  <div class="banner-col banner-col--1">
    <div class="banner-content-wrap">
      <div class="banner-title">
        <span class="h1 banner-heading"><?php echo $banner_title; ?></span>
      </div>
    </div>
  </div>
</header>
