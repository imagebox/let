<?php
/**
 * Displays the 404 page banner
 *
 * @package boxpress
 */

$banner_title         = __( 'Search', 'boxpress' );
$banner_image_url     = '';
$banner_image_width   = '';
$banner_image_height  = '';
$default_banner       = get_field( 'default_banner_image', 'option' );
$search_banner_image  = get_field( 'search_banner_image', 'option' );

if ( $search_banner_image ) {
  $banner_image_url     = $search_banner_image['url'];
  $banner_image_width   = $search_banner_image['width'];
  $banner_image_height  = $search_banner_image['height'];
} elseif ( $default_banner ) {
  $banner_image_url     = $default_banner['url'];
  $banner_image_width   = $default_banner['width'];
  $banner_image_height  = $default_banner['height'];
}

?>
<header class="banner">
  <div class="banner-col banner-col--2">
    <?php if ( ! empty( $banner_image_url ) ) : ?>
      <img class="banner-image" src="<?php echo $banner_image_url; ?>"
        width="<?php echo $banner_image_width; ?>"
        height="<?php echo $banner_image_height; ?>"
        draggable="false"
        alt="">
    <?php endif; ?>
  </div>
  <div class="banner-col banner-col--1">
    <div class="banner-content-wrap">
      <div class="banner-title">
        <span class="h1 banner-heading"><?php echo $banner_title; ?></span>
      </div>
    </div>
  </div>
</header>
