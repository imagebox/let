<?php
/**
 * Template footer
 *
 * Contains the closing of the main el and all content after.
 *
 * @package boxpress
 */
?>
</main>


<footer id="colophon" class="site-footer section" role="contentinfo">
  <div class="wrap">
   <div class="top-footer-box">
     <div class="row">
       <div class="col-xs-12 col-md-8 col-lg-7">
          <?php get_template_part( 'template-parts/global/address-block' ); ?>
       </div>
       <div class="col-xs-12 col-md-4 col-lg-5">
          <?php get_template_part( 'template-parts/global/social-nav' ); ?>
       </div>
     </div>
   </div>
   <div class="bottom-footer-box">
     <div class="row">
       <div class="col-xs-12 col-md-9">
         <div class="nav-box">
           <div class="site-copyright">
             <p>
               <small>
                 <?php _e('', 'boxpress'); ?> &copy; <?php echo date('Y'); ?>
                 <?php
                 $company_name     = get_bloginfo( 'name', 'display' );
                 $alt_company_name = get_field( 'alternative_company_name', 'option' );

                 if ( ! empty( $alt_company_name )) {
                   $company_name = $alt_company_name;
                 }
                 ?>
                 <?php echo $company_name; ?>.
                 <?php _e('All rights Reserved.', 'boxpress'); ?>
               </small>
             </p>
           </div>
           <?php // Footer Navigation ?>
           <?php if ( has_nav_menu( 'footer' )) : ?>
               <nav class="navigation--footer"
                 aria-label="<?php _e( 'Footer Navigation', 'boxpress' ); ?>"
                 role="navigation">
                 <ul class="nav-list">
                   <?php
                     wp_nav_menu( array(
                       'theme_location'  => 'footer',
                       'items_wrap'      => '%3$s',
                       'container'       => false,
                       'walker'          => new Aria_Walker_Nav_Menu(),
                     ));
                   ?>
                 </ul>
               </nav>
           <?php endif; ?>
         </div>
       </div>
       <div class="col-xs-12 col-md-3">
         <div class="imagebox">
           <p>
             <small>
               <?php _e('Website by', 'boxpress'); ?>
               <a href="https://imagebox.com" target="_blank">
                 <span class="vh">Imagebox</span>
                 <svg class="imagebox-logo-svg" width="81" height="23" focusable="false">
                   <use href="#imagebox-logo"/>
                 </svg>
               </a>
             </small>
           </p>
         </div>
       </div>
     </div>
   </div>
  </div>
  </footer>
</div>

<?php // Accessability Toolbar ?>
<div class="accessibility-toolbar <?php
    if ( isset($_COOKIE['is_accessibility_toolbar']) && $_COOKIE['is_accessibility_toolbar'] === 'false' ) {
      echo 'is-closed';
    }
  ?>" role="toolbar" aria-label="Accessability Toolbar">
  <div class="accessibility-toolbar-toggle">
    <button type="button" class="toolbar-toggle-button"
      aria-controls="accessibility-toolbar-body"
      aria-expanded="true"><?php
      if ( isset($_COOKIE['is_accessibility_toolbar']) && $_COOKIE['is_accessibility_toolbar'] === 'false' ) {
        _e('Open', 'boxpress');
      } else {
        _e('Close', 'boxpress');
      }
    ?></button>
  </div>
  <div id="accessibility-toolbar-body" class="accessibility-toolbar-body" aria-hidden="false">
    <div class="toolbar-item">
      <?php echo do_shortcode( '[gtranslate]' ); ?>
    </div>
    <div class="toolbar-item">
      <div class="toolbar-text-size">
        <h5><?php _e('Font Size:', 'boxpress'); ?></h5>
        <div>
          <button type="button" class="js-font-size-decrease fs-control">
            <span class="vh">Increase Font-size</span>
            <span>-</span>
          </button>
          <button type="button" class="js-font-size-increase fs-control">
            <span class="vh">Decrease Font-size</span>
            <span>+</span>
          </button>
          <button type="button" class="js-font-size-reset fs-control">
            <span><?php _e('Reset', 'boxpress'); ?></span>
          </button>
        </div>
      </div>
    </div>
  </div>
</div>


<?php wp_footer(); ?>

<?php // Footer Tracking Codes ?>
<?php the_field( 'footer_tracking_codes', 'option' ); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-MKXNF5G8VE"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-MKXNF5G8VE');
</script>

</body>
</html>
