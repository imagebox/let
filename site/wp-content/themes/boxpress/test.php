<section class="quote-block-layout q-yo section <?php echo $quote_bkg; ?>">

        <?php

          $quote_bkg_slider        = get_sub_field( 'quote_background_slider' );
          $quote_bkg_image_slider  = get_sub_field( 'quote_background_image_slider' );
          $quote_bkg_size   = 'block_full_width';

          $quote_category = get_sub_field('testimonial_tax_term');
          $quote_number = get_sub_field('testimonial_slide_number');



           ?>




           <div class="wrap wrap--limited">
          <div class="q_testimonials">

              <?php
              wp_reset_query();

                $post_query = array(
                  'post_type' => 'testimonials',
                  'posts_per_page' => $quote_number,
                  'tax_query' => array(
                      array (
                          'taxonomy' => 'testimonial_cat',
                          'field' => 'slug',
                          'terms' => $quote_category,
                      )
                  ),
                );
                $post_loop = new WP_Query( $post_query );
              ?>

                <?php while ( $post_loop->have_posts() ) : $post_loop->the_post(); ?>

                    <?php
                    $quote_copy_slider       = get_field( 'quote_copy_slider' );
                    $quote_citation_slider   = get_field( 'quote_citation_slider' );
                    $quote_job_title_slider  = get_field( 'quote_job_title_slider' );
                    $quote_job_title_slider_2  = get_field( 'quote_job_title_slider_2' );
                    ?>


              <blockquote class="quote-block">
                <div class="quote-block-body">
                  <p><?php echo $quote_copy_slider; ?></p>
                </div>

                <?php if ( ! empty( $quote_citation_slider ) ) : ?>

                  <cite class="quote-block-citation">
                    <span class="quote-name"><?php echo $quote_citation_slider; ?></span>
                    <?php if ( ! empty( $quote_job_title_slider )) : ?>
                      <span class="quote-title"><?php echo $quote_job_title_slider; ?></span>
                    <?php endif; ?>
                    <?php if ( ! empty( $quote_job_title_slider_2 )) : ?>
                      <span class="quote-title"><?php echo $quote_job_title_slider_2; ?></span>
                    <?php endif; ?>
                  </cite>
                <?php endif; ?>

              </blockquote>

              <?php endwhile; ?>

              <?php wp_reset_query(); ?>








          </div>

          <?php if ( $quote_bkg_image_slider && $quote_bkg_slider === 'background-image' ) : ?>
            <img class="quote-block-layout-bkg" draggable="false" aria-hidden="true"
              src="<?php echo esc_url( $quote_bkg_image_slider['sizes'][ $quote_bkg_size ] ); ?>"
              width="<?php echo esc_attr( $quote_bkg_image_slider['sizes'][ $quote_bkg_size . '-width' ] ); ?>"
              height="<?php echo esc_attr( $quote_bkg_image_slider['sizes'][ $quote_bkg_size . '-height' ] ); ?>"
              alt="">
          <?php endif; ?>


      </div>
    </section>
